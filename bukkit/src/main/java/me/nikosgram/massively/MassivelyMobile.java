/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.google.common.base.Joiner;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import me.nikosgram.massively.api.chat.MessageColor;
import me.nikosgram.massively.api.chat.MessageType;
import me.nikosgram.massively.api.entity.ExperiencesData;
import me.nikosgram.massively.api.entity.HealthData;
import me.nikosgram.massively.api.entity.ManaData;
import me.nikosgram.massively.api.entity.Mobile;
import me.nikosgram.massively.api.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

@ToString
public class MassivelyMobile implements Mobile {
    protected final Player player;
    protected final MassivelyBukkit massively;
    @Getter
    private final HealthData healthData;
    @Getter
    private final ManaData manaData;
    @Getter
    private final ExperiencesData experiencesData;
    @Getter
    private Optional<Rank> rank = Optional.empty();

    protected MassivelyMobile(MassivelyBukkit massively, Player player) {
        this.massively = massively;
        this.player = player;
        healthData = new MassivelyHealthData(this);
        manaData = new MassivelyManaData(this);
        experiencesData = new MassivelyExperiencesData(this);
        update();
    }

    protected void update() {
        ((MassivelyManaData) manaData).update();
        ((MassivelyExperiencesData) experiencesData).update();
        rank = massively.rankManager.getRank(massively.connector.getString("_rank", "uuid", player.getUniqueId().toString(), "rank").orElse(""));
    }

    @Override
    public UUID getUuid() {
        return player.getUniqueId();
    }

    @Override
    public boolean hasRank() {
        return rank.isPresent();
    }

    @Override
    public boolean hasRank(String rank) {
        if (hasRank()) {
            if (this.rank.get().getId().equalsIgnoreCase(rank)) {
                return true;
            }
            for (Rank parent : this.rank.get().getParents()) {
                if (parent.getId().equalsIgnoreCase(rank)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void changeRank(Rank rank) {
        if (massively.connector.exists("_rank", "uuid", player.getUniqueId().toString())) {
            massively.connector.update("_rank", "uuid", player.getUniqueId().toString(), "rank", rank.getId());
        } else {
            Map<String, String> values = new HashMap<>();
            values.put("uuid", player.getUniqueId().toString());
            values.put("rank", rank.getId());
            massively.connector.insert("_rank", values);
        }
        this.rank = Optional.of(rank);
        manaData.appendMana(manaData.getMaxMana());
    }

    @Override
    public void resetMobile() {
        if (massively.connector.exists("_rank", "uuid", player.getUniqueId().toString())) {
            massively.connector.update("_rank", "uuid", player.getUniqueId().toString(), "rank", "");
        }
        massively.connector.update("_mana", "uuid", player.getUniqueId().toString(), "mana", "0.0");
        massively.connector.update("_experiences", "uuid", player.getUniqueId().toString(), "experiences", "0.0");
        update();
        Bukkit.getScheduler().scheduleSyncDelayedTask(massively, () -> {
            Inventory inventory = Bukkit.createInventory(null, 27, MessageColor.GREEN + "Select your rank.");
            {
                ItemStack stack = new ItemStack(Material.BLAZE_ROD);
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName(MessageColor.DARK_PURPLE + "Mage");
                stack.setItemMeta(meta);
                inventory.setItem(11, stack);
            }
            {
                ItemStack stack = new ItemStack(Material.BOW);
                ItemMeta meta = stack.getItemMeta();
                meta.setDisplayName(MessageColor.DARK_GREEN + "Archer");
                stack.setItemMeta(meta);
                inventory.setItem(15, stack);
            }
            player.openInventory(inventory);
        }, 10);
    }

    @Override
    public String getName() {
        return player.getName();
    }

    @Override
    public boolean hasPermission(String permission) {
        return player.hasPermission(permission);
    }

    @Override
    public void sendMessage(String... messages) {
        sendMessage(MessageType.CHAT, messages);
    }

    @Override
    public void sendMessage(MessageType type, String... messages) {
        for (String message : messages) {
            BukkitUtils.sendMessage(player, message, type);
        }
    }

    @Override
    public boolean isOnline() {
        return player.isOnline();
    }

    @Override
    public boolean executeCommand(String command, String[] parameters) {
        return player.performCommand(command + ' ' + Joiner.on(' ').join(parameters));
    }

    @ToString
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    protected static class MassivelyHealthData implements HealthData {
        private final MassivelyMobile mobile;

        @Override
        public double getMaxHealth() {
            return mobile.player.getMaxHealth();
        }

        @Override
        public void setMaxHealth(double maxHealth) {
            mobile.player.setMaxHealth(maxHealth);
        }

        @Override
        public double getHealth() {
            return mobile.player.getHealth();
        }

        @Override
        public void setHealth(double health) {
            if (health > getMaxHealth()) {
                health = getMaxHealth();
            }
            mobile.player.setHealth(health);
        }

        @Override
        public void appendHealth(double health) {
            setHealth(getHealth() + health);
        }

        @Override
        public void removeHealth(double health) {
            setHealth(getHealth() - health);
        }

        @Override
        public boolean isDead() {
            return mobile.player.isDead();
        }
    }

    @ToString
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    protected static class MassivelyExperiencesData implements ExperiencesData {
        private final MassivelyMobile mobile;
        @Getter
        private double experiences = 0.0;

        protected MassivelyExperiencesData update() {
            experiences = mobile.massively.connector.getDouble("_experiences", "uuid", mobile.getUuid().toString(), "experiences").get();
            return this;
        }

        @Override
        public int getLevel() {
            return MassivelyUtils.estimateLevel(experiences);
        }

        @Override
        public void appendExperiences(double experiences) {
            int oldLevel = getLevel();
            mobile.massively.connector.update(
                    "_experiences",
                    "uuid",
                    mobile.getUuid().toString(),
                    "experiences",
                    String.valueOf(this.experiences += Objects.requireNonNull(experiences))
            );
            int newLevel = getLevel();
            if (mobile.isOnline()) {
                if (oldLevel < newLevel) {
                    mobile.player.getWorld().playSound(mobile.player.getLocation(), Sound.LEVEL_UP, 1, 10);
                    mobile.sendMessage(MessageType.ACTION_BAR, MessageColor.GOLD + ">> " + MessageColor.RED + "LEVEL UP" + MessageColor.GOLD + " <<");
                    Bukkit.getScheduler().scheduleSyncDelayedTask(mobile.massively, () -> mobile.sendMessage(MessageType.ACTION_BAR, MessageColor.GOLD + "> " + MessageColor.RED + "LEVEL UP" + MessageColor.GOLD + " <"), 20);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(mobile.massively, () -> mobile.sendMessage(MessageType.ACTION_BAR, MessageColor.GOLD + "LEVEL UP"), 40);
                    Bukkit.getScheduler().scheduleSyncDelayedTask(mobile.massively, () -> mobile.sendMessage(MessageType.ACTION_BAR, MessageColor.GOLD + "Your new level is " + newLevel), 60);
                    mobile.getManaData().appendMana(mobile.getManaData().getMaxMana());
                } else {
                    mobile.sendMessage(MessageType.ACTION_BAR, MessageColor.GREEN + "+" + experiences + " Experiences");
                    mobile.player.playSound(mobile.player.getLocation(), Sound.ORB_PICKUP, 1, new Random().nextInt(10));
                }
            }
        }
    }

    @ToString
    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    protected static class MassivelyManaData implements ManaData {
        private final MassivelyMobile mobile;
        @Getter
        private double mana = 0.0;

        protected MassivelyManaData update() {
            mana = mobile.massively.connector.getDouble("_mana", "uuid", mobile.getUuid().toString(), "mana").get();
            if (mana > getMaxMana()) {
                mana = getMaxMana();
            }
            return this;
        }

        @Override
        public double getMaxMana() {
            return MassivelyUtils.estimateMaxMana(mobile);
        }

        @Override
        public void appendMana(double mana) {
            this.mana += Objects.requireNonNull(mana);
            if (this.mana > getMaxMana()) {
                this.mana = getMaxMana();
            }
            mobile.massively.connector.update(
                    "_mana",
                    "uuid",
                    mobile.getUuid().toString(),
                    "mana",
                    String.valueOf(this.mana)
            );
        }

        @Override
        public void removeMana(double mana) {
            this.mana -= Objects.requireNonNull(mana);
            if (this.mana < 0.0) {
                this.mana = 0.0;
            }
            mobile.massively.connector.update(
                    "_mana",
                    "uuid",
                    mobile.getUuid().toString(),
                    "mana",
                    String.valueOf(this.mana)
            );
        }

        @Override
        public boolean hasMana(double mana) {
            return this.mana >= mana;
        }
    }
}
