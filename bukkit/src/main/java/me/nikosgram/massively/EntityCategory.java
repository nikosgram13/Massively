/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import lombok.Getter;
import org.bukkit.entity.EntityType;

public enum EntityCategory {
    Passive(EntityType.PIG, EntityType.BAT, EntityType.CHICKEN, EntityType.COW, EntityType.MUSHROOM_COW,
            EntityType.RABBIT, EntityType.SHEEP, EntityType.SQUID, EntityType.VILLAGER),
    Neutral(EntityType.CAVE_SPIDER, EntityType.ENDERMAN, EntityType.SPIDER, EntityType.PIG_ZOMBIE),
    Hostile(EntityType.BLAZE, EntityType.CREEPER, EntityType.GUARDIAN, EntityType.ENDERMITE, EntityType.GHAST,
            EntityType.MAGMA_CUBE, EntityType.SILVERFISH, EntityType.SKELETON, EntityType.SLIME, EntityType.WITCH,
            EntityType.WITHER_SKULL, EntityType.ZOMBIE),
    Tamable(EntityType.HORSE),
    Utility(EntityType.IRON_GOLEM, EntityType.SNOWMAN),
    Boss(EntityType.ENDER_DRAGON, EntityType.WITHER),
    Unused(EntityType.GIANT),
    None();

    @Getter
    private final EntityType[] entities;

    EntityCategory(EntityType... entities) {
        this.entities = entities;
    }

    public static EntityCategory getCategory(EntityType type) {
        for (EntityCategory category : values()) {
            for (EntityType entity : category.getEntities()) {
                if (entity.equals(type)) return category;
            }
        }
        return None;
    }
}