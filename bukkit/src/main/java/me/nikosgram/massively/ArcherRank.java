/*
 * Copyright (c) 2015 Nikos Grammatikos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.nikosgram.massively;

import com.darkblade12.particleeffect.ParticleEffect;
import lombok.Getter;
import me.nikosgram.massively.api.entity.Mobile;
import me.nikosgram.massively.api.rank.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("unused")
public class ArcherRank implements Rank, Listener {
    private final MassivelyBukkit massively;
    @Getter
    private final String name = "Archer";
    @Getter
    private final String id = "archer";
    @Getter
    @SuppressWarnings("all")
    private final Collection<Rank> parents = new ArrayList<>();
    @Getter
    private final double health = 20;
    @Getter
    private final double startMana = 100;
    @Getter
    private final double appendManaPerLevel = 15;
    @Getter
    private final double appendManaPerRefresh = 3;
    private boolean registered = false;

    protected ArcherRank(MassivelyBukkit massively) {
        this.massively = massively;
        if (massively.rankManager.contains("elf")) {
            parents.add(massively.rankManager.getRank("elf").get());
        }
    }

    @Override
    public void registerEvents() {
        if (registered) {
            throw new RuntimeException();
        }
        massively.getServer().getPluginManager().registerEvents(this, massively);
        registered = true;
    }

    @EventHandler
    public void onProjectileLaunch(ProjectileLaunchEvent event) {
        if (event.getEntity() instanceof Arrow) {
            Arrow arrow = (Arrow) event.getEntity();
            if (arrow.getShooter() instanceof Player) {
                Player player = (Player) arrow.getShooter();
                if (player.isSneaking()) {
                    Mobile mobile = massively.mobileManager.getMobile(player.getUniqueId()).get();
                    if (mobile.hasRank(id)) {
                        double neededMana = MassivelyUtils.estimateNeededMana(mobile, 40);
                        if (mobile.getManaData().hasMana(neededMana)) {
                            arrow.setCritical(true);
                            arrow.setMetadata("explosion", new FixedMetadataValue(massively, true));
                        } else {
                            mobile.sendMessage("You don't have enough mana. Needs " + neededMana + " and you have " + mobile.getManaData().getMana() + '/' + mobile.getManaData().getMaxMana());
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onHitEntity(EntityDamageByEntityEvent event) {
        if (!event.getEntity().hasMetadata("bypass")) {
            event.getEntity().removeMetadata("bypass", massively);
            if (event.getDamager() instanceof Arrow) {
                Arrow arrow = (Arrow) event.getDamager();
                if (arrow.getShooter() instanceof Player) {
                    double damage = event.getDamage();
                    Mobile mobile = massively.mobileManager.getMobile(((Player) arrow.getShooter()).getUniqueId()).get();
                    if (mobile.hasRank(id)) {
                        event.setDamage(event.getDamage() + (1.25 * mobile.getExperiencesData().getLevel()));
                    }
                    mobile.sendMessage(event.getFinalDamage() + "/" + damage);
                }
            } else if (event.getDamager() instanceof Player) {
                double damage = event.getDamage();
                Mobile mobile = massively.mobileManager.getMobile(event.getDamager().getUniqueId()).get();
                if (mobile.hasRank(id)) {
                    event.setDamage((event.getDamage() / 100) * 40); //-60%
                }
                mobile.sendMessage(event.getFinalDamage() + "/" + damage);
            }
        }
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        if (event.getEntity() instanceof Arrow) {
            Arrow arrow = (Arrow) event.getEntity();
            if (arrow.getShooter() instanceof Entity) {
                if (arrow.hasMetadata("explosion")) {
                    arrow.getNearbyEntities(2.5, 2.5, 2.5).stream().filter(entity -> entity instanceof LivingEntity).forEach(entity -> {
                        entity.setMetadata("bypass", new FixedMetadataValue(massively, true));
                        ((LivingEntity) entity).damage(50, ((Entity) arrow.getShooter()));
                    });
                    arrow.getWorld().createExplosion(arrow.getLocation(), 0);
                    arrow.remove();
                }
            }
        }
    }

    @EventHandler
    public void onClickBone(PlayerInteractEvent event) {
        if (!event.getAction().equals(Action.PHYSICAL)) {
            if (event.getItem() != null) {
                switch (event.getItem().getType()) {
                    case BONE:
                        if (event.getItem().getItemMeta().getLore() != null) {
                            if (event.getItem().getItemMeta().getLore().contains("wolf_army")) {
                                Mobile mobile = massively.mobileManager.getMobile(event.getPlayer().getUniqueId()).get();
                                if (mobile.hasRank(id)) {
                                    double neededMana = MassivelyUtils.estimateNeededMana(mobile, 70);
                                    if (mobile.getManaData().hasMana(neededMana)) {
                                        Wolf wolf = event.getPlayer().getWorld().spawn(event.getPlayer().getLocation(), Wolf.class);
                                        wolf.setOwner(event.getPlayer());
                                        mobile.getManaData().removeMana(neededMana);
                                    } else {
                                        mobile.sendMessage("You don't have enough mana. Needs " + neededMana + " and you have " + mobile.getManaData().getMana() + '/' + mobile.getManaData().getMaxMana());
                                    }
                                }
                            }
                        }
                        break;
                    case FIREWORK_CHARGE:
                        if (event.getItem().getItemMeta().getLore() != null) {
                            if (event.getItem().getItemMeta().getLore().contains("invincible_star")) {
                                if (event.getPlayer().isSneaking()) {
                                    Mobile mobile = massively.mobileManager.getMobile(event.getPlayer().getUniqueId()).get();
                                    if (mobile.hasRank(id)) {
                                        double neededMana = MassivelyUtils.estimateNeededMana(mobile, 40);
                                        if (mobile.getManaData().hasMana(neededMana)) {
                                            ParticleEffect.PORTAL.display(
                                                    0.5F, 0.5F, 0.5F, 0, 10, event.getPlayer().getLocation().add(0, 1, 0), 5
                                            );
                                            Bukkit.getOnlinePlayers().forEach(player -> player.hidePlayer(event.getPlayer()));
                                            event.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 2, false), true);
                                            Bukkit.getScheduler().scheduleSyncDelayedTask(massively, () -> {
                                                if (event.getPlayer().isOnline()) {
                                                    ParticleEffect.PORTAL.display(
                                                            0.5F, 0.5F, 0.5F, 0, 10, event.getPlayer().getLocation().add(0, 1, 0), 5
                                                    );
                                                    Bukkit.getOnlinePlayers().forEach(player -> player.showPlayer(event.getPlayer()));
                                                }
                                            }, 100);
                                        } else {
                                            mobile.sendMessage("You don't have enough mana. Needs " + neededMana + " and you have " + mobile.getManaData().getMana() + '/' + mobile.getManaData().getMaxMana());
                                        }
                                        event.setCancelled(true);
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    @EventHandler
    public void onKillEntity(EntityDeathEvent event) {
        if (event.getEntity().getKiller() != null) {
            Player killer = event.getEntity().getKiller();
            Mobile mobile = massively.mobileManager.getMobile(killer.getUniqueId()).get();
            if (mobile.hasRank(id)) {
                ExperienceOrb orb = killer.getWorld().spawn(killer.getLocation(), ExperienceOrb.class);
                orb.setExperience(event.getDroppedExp());
                event.setDroppedExp(0);
            }
        }
    }
}
